package max;

import java.util.Scanner;

public class MaxDigit {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("The number for which the maximum digit should be calculated is: ");
		int numb = in.nextInt();
		
		int maxx = numb % 10;
		
		while(numb != 0){
			if(numb % 10 >= maxx)
				maxx = numb % 10;
			numb /= 10;
		}
		
		System.out.println("The maximum digit of the given number is: " + maxx);

		in.close();
	}

}